package com.amzi.servlets;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
//import java.sql.*;
//import java.util.*;


public class IssueRegisterServlet extends HttpServlet{

    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  

        response.setContentType("text/html");    
        
        String n=request.getParameter("title");  
        String p=request.getParameter("body");
        String q=request.getParameter("createdby");
        String r=request.getParameter("status");
        
        HttpSession session = request.getSession(false);
        if(session!=null)
        session.setAttribute("name", n);
       
       
        Connection conn = null;

        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "users";
        String driver = "com.mysql.jdbc.Driver";
        String Name = "root";
        String pass = "0945";
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager
                    .getConnection(url + dbName, Name, pass);
            System.out.println("Connected database successfully...");
            String query = " insert into issue1 (title,body,createdby,status)"
                    + " values (?, ?, ?, ?)";
             
                  
                  PreparedStatement preparedStmt = conn.prepareStatement(query);
                  preparedStmt.setString (1, n);
                  preparedStmt.setString (2, p);
                  preparedStmt.setString   (3, q);
                  preparedStmt.setString   (4, r);
             
                  
                  preparedStmt.execute();
                   
                  conn.close();
                }
                catch (Exception e)
                {
                  System.err.println("Got an exception!");
                  System.err.println(e.getMessage());
                }
        RequestDispatcher rd=request.getRequestDispatcher("IssueSucess.jsp");  
        rd.forward(request,response); 
    }  
} 